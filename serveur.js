var http = require('http');
var url = require('url');
var querystring = require('querystring'); // gestion de récupération des paramètres

var server = http.createServer(function(req, res) {
    var params = querystring.parse(url.parse(req.url).query); // récup des paramètres
    res.writeHead(200, {"Content-Type": "text/plain"});
    if ('prenom' in params && 'nom' in params) {
        res.write('Vous vous appelez ' + params['prenom'] + ' ' + params['nom']);
    }
    else {
        res.write('Vous devez bien avoir un prénom et un nom, non ?');
    }
    res.end();
});
server.listen(8080);